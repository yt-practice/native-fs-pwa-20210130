// const path = require('path')
const withPreact = require('next-plugin-preact')
const basePath =
	'development' === process.env.NODE_ENV ? '' : '/native-fs-pwa-20210130/'
module.exports = withPreact({
	basePath: basePath.replace(/\/$/, ''),
	assetPrefix: basePath,
	publicRuntimeConfig: {
		basePath,
	},
	experimental: {
		optimizeFonts: true,
		optimizeImages: true,
	},
})
