import localforage from 'localforage'

let st: Promise<Store> | undefined
export const getStore = async () => {
	if (st) return st
	st = Store.create()
	return st
}

class Store {
	readonly dir: DirectoryHandle | undefined
	static async create() {
		const ins = localforage.createInstance({
			driver: localforage.INDEXEDDB,
			name: 'app',
			version: 1,
			storeName: 'appstore',
		})
		const s = new Store(ins)
		await s.init()
		return s
	}
	private constructor(private readonly st: LocalForage) {}
	async init() {
		// @ts-expect-error: init
		this.dir = (await this.st.getItem('dir')) as DirectoryHandle
		// if (dir) return dir
	}
	async setdir() {
		// @ts-expect-error: ex
		const hd = (await showDirectoryPicker()) as DirectoryHandle
		// @ts-expect-error: init
		this.dir = hd
		await this.st.setItem('dir', hd)
	}
	async *walk() {
		if (!this.dir) throw new Error('dir is required.')
		yield* walkdir(this.dir, [])
	}
}

const walkdir = async function* (
	hd: DirectoryHandle,
	p: string[],
): AsyncIterable<FileItem> {
	for await (const v of hd.values()) {
		if ('directory' === v.kind) yield* walkdir(v, [...p, v.name])
		else yield new FileItem(v, p)
	}
}

export class FileItem {
	constructor(
		private readonly _file: FileHandle,
		private readonly _path: string[],
	) {}
	get path() {
		return [...this._path, this._file.name].join('/')
	}
	get name() {
		return this._file.name
	}
	async read() {
		return this._file.getFile()
	}
	async verifyPermission(params: { request?: boolean } = {}) {
		const opts = {}
		const { request = true } = params

		// Check if we already have permission, if so, return true.
		if ('granted' === (await this._file.queryPermission(opts))) {
			return true
		}

		// Request permission to the file, if the user grants permission, return true.
		if (request)
			if ('granted' === (await this._file.requestPermission(opts))) {
				return true
			}

		// The user did not grant permission, return false.
		return false
	}
}

export type Handle = DirectoryHandle | FileHandle

interface IHandle {
	readonly kind: 'file' | 'directory'
	readonly name: string
	isSameEntry(hd: Handle): boolean
}

export interface DirectoryHandle extends IHandle {
	readonly kind: 'directory'
	getFileHandle(name: string): FileHandle
	getDirectoryHandle(name: string): DirectoryHandle
	entries(): AsyncIterable<[name: string, value: Handle]>
	values(): AsyncIterable<Handle>
}

export interface FileHandle extends IHandle {
	readonly kind: 'file'
	getFile(): File
	queryPermission(opts: {
		mode?: 'read' | 'readwrite'
	}): Promise<'granted' | 'denied' | 'prompt'>
	requestPermission(opts: {
		mode?: 'read' | 'readwrite'
	}): Promise<'granted' | 'denied' | 'prompt'>
}
