import { useEffect, useState } from 'react'
import { getStore, FileItem } from '~/lib/store'

type Detail = {
	file: FileItem
	text: string
}

const ShowDetail = ({ detail }: { detail: Detail }) => {
	return (
		<div>
			<h2>{detail.file.path}</h2>
			<p>{detail.text}</p>
		</div>
	)
}

const Index = () => {
	const [list, setlist] = useState(null as null | FileItem[])
	const [detail, setdetail] = useState(null as null | Detail)
	const setdir = async () => {
		await getStore().then(async store => {
			await store.setdir()
			const list: FileItem[] = []
			for await (const p of store.walk()) list.push(p)
			setlist(list)
			setdetail(null)
		})
	}
	const clear = () => {
		setlist(null)
		setdetail(null)
	}
	const show = (file: FileItem) => (ev: { preventDefault: () => void }) => {
		ev.preventDefault()
		Promise.all([
			file
				.verifyPermission()
				.then(() => file.read().then(r => r.arrayBuffer()))
				.catch(x => {
					console.error(x)
				}),
			import('encoding-japanese'),
		]).then(([buf, { convert }]) => {
			if (!buf) return alert('開くのに失敗した')
			try {
				const arr = new Uint8Array(buf)
				const text = convert(arr, {
					to: 'UNICODE',
					from: 'SJIS',
					type: 'string',
				})
				setdetail({ file, text })
			} catch (x) {
				console.error(x)
				alert('デコードに失敗した')
			}
		})
	}
	useEffect(() => {
		getStore().then(async store => {
			if (!store.dir) return
			const list: FileItem[] = []
			for await (const p of store.walk()) list.push(p)
			setlist(list)
			setdetail(null)
		})
	}, [])
	return (
		<div style={{ fontFamily: 'monospace' }}>
			<p>
				<button onClick={setdir}>set directory</button>
				<button onClick={clear}>clear</button>
			</p>
			<hr />
			{detail && <ShowDetail detail={detail} />}
			<hr />
			{detail || list ? null : (
				<p>sjis のテキストファイルの入ったディレクトリを選択してください。</p>
			)}
			{list?.map(item => (
				<p>
					<a href="#" onClick={show(item)}>
						{item.path}
					</a>
				</p>
			))}
		</div>
	)
}

export default Index
